FROM openjdk:11
COPY build/libs/mygifservice-0.0.1-SNAPSHOT.jar /tmp
WORKDIR /tmp
CMD java -jar ./mygifservice-0.0.1-SNAPSHOT.jar